<div class="form-group">
    <label for="form">附件</label>
    <table class="table">
        <tbody id="files_tb">
        @foreach ($files as $item)        
        <tr>
            <td><a target="_blank" href="{{url($item->file)}}">{{$item->name}}</a></td>
            <td>
                <button type="button" class="btn btn-danger pull-right js_del_files" onclick="delFile(this)" data-file-url="{{$item->file}}">刪除</button>
            </td>
        </tr>
        @endforeach
        @if (old('pdf_files'))        
        @foreach (old('pdf_files') as $key => $file)        
        <tr>
            <td><a target="_blank" href="{{url($file)}}">{{old('pdf_names')[$key]}}</a></td>
            <td>
                <input type="hidden" name="pdf_files[]" value="{{$file}}" />
                <input type="hidden" name="pdf_names[]" value="{{old('pdf_names')[$key]}}" />
                <button type="button" class="btn btn-danger pull-right js_del_files" onclick="delFile(this)" data-file-url="{{$file}}">刪除</button>
            </td>
        </tr>
        @endforeach
        @endif
        </tbody>
    </table>
</div>
<div class="form-group">
    <div class="input-group">
        <span class="input-group-addon">文件名称</span>
        <input type="text" class="form-control" id="display_name">
        <span class="input-group-btn">
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Add files...</span>
                <input id="fileupload" type="file" name="files[]">
            </span>
        </span>
    </div>
</div>

<link rel="stylesheet" href="/javascripts/jQueryFileUpload/css/jquery.fileupload.css">
<link rel="stylesheet" href="/javascripts/jQueryFileUpload/css/jquery.fileupload-ui.css">

<script type="text/javascript"  src="/javascripts/jQueryFileUpload/js/vendor/jquery.ui.widget.js"></script>
<script type="text/javascript"  src="/javascripts/jQueryFileUpload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/javascripts/jQueryFileUpload/js/jquery.fileupload.js"></script>

<script type="text/template" id="html_template">
<tr>
    <td><a target="_blank" href="<%url%>"><%name%></a></td>
    <td>
        <input type="hidden" name="pdf_files[]" value="<%path%>" />
        <input type="hidden" name="pdf_names[]" value="<%name%>" />
        <button type="button" class="btn btn-danger pull-right js_del_files" onclick="delFile(this)" data-file-url="<%path%>">刪除</button>
    </td>
</tr>
</script>

<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var template = $('#html_template').html();
var TemplateEngine = function(tpl, data) {
    var re = /<%([^%>]+)?%>/g;
    while(match = re.exec(tpl)) {
        tpl = tpl.replace(match[0], data[match[1]])
    }
    return tpl;
}

$(function () {
    $('#fileupload').fileupload({
        dataType: 'json',
        url: '/file/upload',
        done: function (e, data) {
            var filesFrame = [];
            $.each(data.result.files, function (index, file) {
                var tpl = TemplateEngine(template, file);
                filesFrame.push(tpl);
            });
            $('#files_tb').append(filesFrame.join(''));
        }
    });

    $('#fileupload').bind('fileuploadsubmit', function (e, data) {
        var $input = $('#display_name');
        data.formData = {name: $input.val()};

        if (!data.formData.name) {
            alert('請輸入文件名稱');
            $input.focus();
            return false;
        }
    });
});
function delFile(obj) {
    var $self = $(obj);
    var params = {fileUrl: $self.data('file-url')};

    $.ajax({
        url: '/file/del/',
        type: 'POST',
        dataType: 'json',
        data: params
    })
    .done(function() {
        $self.parents('tr').remove();
        console.log("success");
    })
    .fail(function() {
        console.log("error");
    })
}
</script>