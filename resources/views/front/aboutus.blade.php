@extends('front.layouts.default')

@section('page_css')
<link href="/styles/common/style_activitylist.css" rel="stylesheet" type="text/css" />
<link href="/styles/common/style_heart.css" rel="stylesheet" type="text/css" />
@stop


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-18">
            <div class="divMainTitle">
                <img src="/images/zh_TW/aboutus/title_aboutus.png">
            </div>        
        </div>
        <div class="col-md-18">
            <div class="row divMessageBox">
                <div class="col-md-18">
                    <div class="divMessageBoxTop"></div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxContent">
                        正念禪修中心有限公司（公司註冊編號 1543765）成立於 2010 年 12 月 22 日，是一家非牟利機構，並已按香港《稅務條例》第 88 條登記獲豁免繳稅。
                        <br>
                        <br> 本中心鼎承佛陀的使命，弘法利生的理念開設平臺，促使有緣者能在修習佛法的道路上結識善知識，與善人共聚討論佛法，以增益自身菩提道業。中心推廣上座部佛教的正念禪修及籌辦有關慈善活動，包括佛法講座、禪修營、短期出家、刊物出版（包括書報與電子版）等。
                        <br>
                        <br> 宏願
                        <br> 推廣佛陀正法藉以陶冶眾生，喚醒世人的悟性，回歸自然的靈性生活，並提升個人品德，以促進社會的安寧與祥和，維護世界和平。
                        <br>
                        <br> 理念
                        <br> 1　善緣同聚：敦厚相待、同心協力推動會務。
                        <br> 2　信心同建：深信三寶、篤實善士樹植信念。
                        <br> 3　淨行同持：佈施供養、持戒禪定淨化身心。
                        <br> 4　法義同解：廣學多聞、如理思義妙解法理。
                        <br> 5　智慧同證：解行並進、止觀雙修園成內智。
                        <br>
                        <br> 宗旨
                        <br> 1　以道趣入：引導學員解行並重，瞭解修行的次第步驟，奠基聞思，建立正知正見，掌握佛法的核心。
                        <br> 2　以理攝眾：灌輸學員正確的佛教義理，回歸佛陀的本懷，契入如法的生活。
                        <br> 3　以德律己：強調道德意識的思想，引導學員依循安穩之戒法，生慈心、修學自利利他的正行。
                        <br>
                        <br> 使命
                        <br> 1　開設禪修班、密集禪修營、佛法生活營等，讓學員有機會鍛煉清醒的明覺，藉以淨化心靈。
                        <br> 2　開辦佛學班、佛法專題講座，以及播放契合佛法的影片，並借助以音樂、戲劇、舞蹈文娛等弘揚佛法。
                        <br> 3　出版一系列的佛法叢書及製作影音光碟，供眾結緣流通。
                    </div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxBottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="divPopup" class="container">
    <div id="divPopupInner">
        <div id="divPopupInnerArea" class="embed-responsive embed-responsive-4by3">
              <iframe class="embed-responsive-item" src="">Loading…</iframe>
        </div>
        <div class="text-center">
            <img class="imgButton divPopup_close" src="images/zh_TW/publication/btn_close.png" />
        </div>
    </div>
</div>
@stop

@section('page_js')
<script type="text/javascript">

$("#divPopup").popup({
    transition: 'all 0.3s'
});

function updatePopup(aObj) {
    var htmlCode = $(aObj).parent().find("input[type='hidden']").val();
    $("#divPopupInnerArea").find('iframe').attr('src', htmlCode);
}
</script>
@stop