@extends('front.layouts.default')

@section('page_css')
<link href="/styles/common/style_activitylist.css" rel="stylesheet" type="text/css" />
<link href="/styles/common/style_productlist.css" rel="stylesheet" type="text/css" />
@stop


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-18">
            <div class="divMainTitle">
                <img src="/images/zh_TW/productlist/title_boutique.png">
            </div>        
        </div>
        <div class="col-md-18">
            <div class="row divMessageBox">
                <div class="col-md-18">
                    <div class="divMessageBoxTop"></div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxContent divShop">
                        <div class="divAddress">
                            地址：正念禪修中心 香港荃灣海盛路3號TML廣場16樓D5室
                        </div>
                        <div class="table divInfo">
                            <div class="tableRow">
                                <div class="tableCellTop">
                                    <div class="tableCellMiddle">
                                        營業時間：星期一至日10am-5pm
                                    </div>
                                    <div class="tableCellMiddle">
                                        查詢電話：+852 3563 8913
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-2  divCategoryCell">
                                <div class="alignLeft divCategory @if($id) noArrow @endif ">
                                    @if(!$id)
                                    <div class="tableCellMiddle" style="padding:0 5px 0 0; width:15px;">
                                        <img src="/images/common/productlist/img_boutique_menu_arrow.png">
                                    </div>
                                    @endif
                                    <div class="tableCellMiddle">
                                        <a href="{{action('FrontController@getProductlist', ['id' => 0])}}"><div class="break-word">全部</div></a>
                                    </div>
                                </div>
                                @foreach ($categories as $item)                                
                                <div class="alignLeft divCategory @if($id != $item->id) noArrow @endif">
                                    @if($id == $item->id)
                                    <div class="tableCellMiddle" style="padding:0 5px 0 0; width:15px;">
                                        <img src="/images/common/productlist/img_boutique_menu_arrow.png">
                                    </div>
                                    @endif
                                    <div class="tableCellMiddle">
                                        <a href="{{action('FrontController@getProductlist', ['id' => $item->id])}}"><div class="break-word">{{$item->name_cht}}</div></a>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="col-xs-12 col-sm-16">
                                <div class="row">
                                    @foreach ($list as $item)
                                    <div class="col-sm-6">                                
                                        <div class="divProductBlock">
                                            <div class="divProductImage"><img src="{{url($item->photo)}}" onclick="updatePopup(this);" class="imgButton divPopup_open img-responsive">
                                            </div>
                                            <div class="divProductTitle"><span class="txtProductTitle break-word">{{$item->name_cht}}</span>
                                            </div>
                                            <input type="hidden" id="view1:form:j_id_x:0:j_id_13" name="view1:form:j_id_x:0:j_id_13" value="{!!$item->intro_cht!!}">
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div id="divPagination" class="text-center">
                            {!! $list->render() !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxBottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="divPopup" class="container">
    <div id="divPopupInner">
        <div class="table" style="width:100%;">
            <div class="tableRow">
                <div class="tableCellTop" style="padding:0 20px 0 0;">
                    <div id="divPopupTitle"></div>
                    <div id="divPopupDesc1Container">
                        <div id="divPopupDesc1Title">
                            產品介紹：
                        </div>
                        <div id="divPopupDesc1">
                        </div>
                    </div>
                </div>
                <div class="tableCellTop">
                    <div id="divPopupImage" onclick="showLargeImage(this);">
                        <img id="imgPopupImage" class="img-responsive" />
                        <div class="divZoomImage"></div>
                    </div>
                </div>
            </div>
        </div>
        <div style="text-align:center; padding:20px 0 0 0;">
            <img class="imgButton divPopup_close" src="/images/zh_TW/publication/btn_close.png" />
        </div>
    </div>
    <div id="divPopupInnerImage" style="display:none;">
        <div style="text-align:center; padding:20px 0 0 0;">
            <img id="imgPopupImageLarge" />
        </div>
        <div style="text-align:center; padding:20px 0 0 0;">
            <img class="imgButton" src="/images/zh_TW/publication/btn_close.png" onclick="hideLargeImage();" />
        </div>
    </div>
</div>
@stop

@section('page_js')
<script type="text/javascript">

$("#divPopup").popup({
    transition: 'all 0.3s'
});

function updatePopup(aObj) {
    
    hideLargeImage();
    
    var title = $(aObj).parent().parent().find(".txtProductTitle").html().trim();
    $("#divPopupTitle").html(title);
    
    var desc1 = $(aObj).parent().parent().find("input[type='hidden']").val();
    
    if (isBlankString(desc1)==false) {
        $("#divPopupDesc1Container").show();
        $("#divPopupDesc1").html(desc1);
    } else {
        $("#divPopupDesc1Container").hide();
    }
    
    $("#imgPopupImage").attr("src", $(aObj).attr("src"));
    $("#aPopupImage").attr("href", $(aObj).attr("src"));
    
}

function showLargeImage(aObj) {
    var imgSrc = $(aObj).find("img").attr("src");
    // imgSrc = imgSrc.replace("large", "original");
    $("#imgPopupImageLarge").attr("src", imgSrc);
    
    $("#divPopupInner").slideUp(400);
    $("#divPopupInnerImage").slideDown(400);
}
function hideLargeImage() {
    $("#divPopupInnerImage").slideUp(400);
    $("#divPopupInner").slideDown(400);
}
</script>
@stop