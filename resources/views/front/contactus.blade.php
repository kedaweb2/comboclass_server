@extends('front.layouts.default')

@section('page_css')
<link href="/styles/common/style_activitylist.css" rel="stylesheet" type="text/css" />
<link href="/styles/common/style_contactus.css" rel="stylesheet" type="text/css" />
@stop


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-18">
            <div class="divMainTitle">
                <img src="./images/zh_TW/contactus/title_contactus.png">
            </div>        
        </div>
        <div class="col-md-18 divContactUs">
            <div class="row divMessageBox">
                <div class="col-md-18">
                    <div class="divMessageBoxTop"></div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxContent">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d922.3696920727441!2d114.10632393464518!3d22.373301721192533!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3403f8e53cf3d493%3A0xbadee497da3084f5!2zVCBNIEwg5buj5aC0!5e0!3m2!1szh-TW!2shk!4v1403047549481" class="embed-responsive-item" ></iframe>
                        </div>
                        <div class="divSubTitle">正念禪修中心</div>
                        <div class="divMessageContent">
                            <div class="">
                                <div class="tableRow">
                                    <div class="tableCellTop divLeftCell">
                                        地址：
                                    </div>
                                    <div class="tableCellTop">
                                        香港荃灣海盛路3號TML廣場16樓D5室
                                    </div>
                                </div>
                                <div class="tableRow">
                                    <div class="tableCellTop divLeftCell">
                                        電話：
                                    </div>
                                    <div class="tableCellTop">
                                        (852) 3563 8913
                                    </div>
                                </div>
                                <div class="tableRow">
                                    <div class="tableCellTop divLeftCell">
                                        Whatsapp或微信：
                                    </div>
                                    <div class="tableCellTop">
                                        (852) 9697 2768
                                    </div>
                                </div>
                                <div class="tableRow">
                                    <div class="tableCellTop divLeftCell">
                                        電郵：
                                    </div>
                                    <div class="tableCellTop">
                                        <a href="mailto:dhammamittahk@gmail.com">dhammamittahk@gmail.com</a>
                                    </div>
                                </div>
                                <div class="tableRow">
                                    <div class="tableCellTop divLeftCell">
                                        Facebook：
                                    </div>
                                    <div class="tableCellTop">
                                        <a href="http://www.facebook.com/CentreofMindfulness" target="_blank">http://www.facebook.com/CentreofMindfulness</a>
                                    </div>
                                </div>
                                <div class="tableRow">
                                    <div class="tableCellTop divLeftCell">
                                        查詢時間：
                                    </div>
                                    <div class="tableCellTop">
                                        <div class="table">
                                            <div class="tableRow">
                                                <div class="tableCellTop">
                                                    星期一
                                                </div>
                                                <div class="tableCellTop divTimeRightCell">
                                                    下午2時至下午6時
                                                </div>
                                            </div>
                                            <div class="tableRow">
                                                <div class="tableCellTop">
                                                    星期二至星期五
                                                </div>
                                                <div class="tableCellTop divTimeRightCell">
                                                    上午10時至下午6時
                                                </div>
                                            </div>
                                            <div class="tableRow">
                                                <div class="tableCellTop">
                                                    星期六、星期日
                                                </div>
                                                <div class="tableCellTop divTimeRightCell">
                                                    上午10時至下午5時
                                                </div>
                                            </div>
                                        </div>
                                        逢公眾假期放香（活動安排或更改，將於Facebook專頁公布）
                                    </div>
                                </div>
                                <div class="tableRow">
                                    <div class="tableCellTop divLeftCell">
                                        交通資訊：
                                    </div>
                                    <div class="tableCellTop">
                                        乘港鐵（MTR）荃灣綫到荃灣站，從地下D出口出閘後右轉住前行約一百米，轉乘96B小巴至柴灣角街下車，穿過柴灣角熟食市場，再往前行5分鐘到達。
                                        <br /> 乘西鐵在荃灣西站A2（有線電視大廈）出口，向海盛路前往，前行大約10分鐘到達。
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxBottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="divPopup" class="container">
    <div id="divPopupInner">
        <div id="divPopupInnerArea" class="embed-responsive embed-responsive-4by3">
              <iframe class="embed-responsive-item" src="">Loading…</iframe>
        </div>
        <div class="text-center">
            <img class="imgButton divPopup_close" src="images/zh_TW/publication/btn_close.png" />
        </div>
    </div>
</div>
@stop

@section('page_js')

<script type="text/javascript">

$("#divPopup").popup({
    transition: 'all 0.3s'
});

function updatePopup(aObj) {
    var htmlCode = $(aObj).parent().find("input[type='hidden']").val();
    $("#divPopupInnerArea").find('iframe').attr('src', htmlCode);
}
</script>
@stop