<div class="container" id="divFooterInner">
    <div class="row">
        <div class="col-md-18"></div>
        <div class="col-md-18">
            <ul class="list-inline pull-right">
                <li><a href="{{action('FrontController@getAboutus')}}">關於我們</a></li>
                <li><a href="{{action('FrontController@getActivitylist')}}">活動</a></li>
                <li><a href="{{action('FrontController@getCourselist')}}">課程</a></li>
                <li><a href="{{action('FrontController@getGallery')}}">相集</a></li>
                <li><a href="{{action('FrontController@getPublication')}}">出版</a></li>
                <li><a href="{{action('FrontController@getHeart')}}">心靈聆聽</a></li>
                <li><a href="{{action('FrontController@getProductlist',['id' => 0])}}">禪悅文化</a></li>
                <li><a href="{{action('FrontController@getDonation')}}">發心護持</a></li>
                <li><a href="{{action('FrontController@getContactus')}}">聯絡我們</a></li>
            </ul>
        </div>
        <div class="col-md-18">
            <div class="table divFooterCopyright"><span class="txtCopyright">版權由正念禪修中心所有 歡迎轉載</span></div>
        </div>
    </div>
</div>