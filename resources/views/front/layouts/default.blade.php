<!DOCTYPE html>
<html>
<head>
<title>正念禪修中心</title>

<meta http-equiv="content-type" content="text/html;charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="renderer" content="webkit">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href="/styles/jquery.ui.css" type="text/css" rel="stylesheet"/>
<link href="/javascripts/impromptu/jquery-impromptu.css" rel="stylesheet" type="text/css" />
<link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/styles/common/style_common.css" rel="stylesheet" type="text/css" />
<link href="/styles/common/style.css" rel="stylesheet" type="text/css" />
<link href="/styles/zh_TW/style.css" rel="stylesheet" type="text/css" />
@yield('page_css')

<script src="/javascripts/common.js"></script>
<script src="/javascripts/jquery-1.9.1.min.js"></script>
<script src="/javascripts/jquery.cookie.js"></script>
<script src="/javascripts/jquery.custom.js"></script>
<script src="/javascripts/impromptu/jquery-impromptu.js"></script>
<script src="/javascripts/jquery.marquee.min.js"></script>
<script src="/javascripts/slidejs/jquery.slides.js"></script>
<script src="/javascripts/jquery.popupoverlay.js"></script>
<script src="/assets/bootstrap/js/bootstrap.min.js"></script>
</head>




<body>
@include('front.layouts.nav')
<div class="contentBody">
    @yield('content')
    @include('front.layouts.foot_nav')
    <!-- <div id="divFooterBackground"></div> -->
</div>

<script type="text/javascript">
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});
$(document).ready(function() {
    showDialog("", "");
});
</script>
@yield('page_js')
</body>
</html>