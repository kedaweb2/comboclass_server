<div id="divHeader" class="hidden-xs">
    <div id="divHeaderArea" class="container">
        <div class="row" id="divHeaderAreaContent">
            <div id="divLogo" class="pull-left col-sm-1">
                <a href="{{action('FrontController@getIndex')}}"><img src="/images/common/header/img_logo.png" /></a>
            </div>
            <div id="divMenu" class="pull-right col-sm-17">
                <ul class="list-inline text-right">
                    <li>
                        <a href="{{action('FrontController@getAboutus')}}"><img src="/images/zh_TW/header/menu/btn_menu_aboutus.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_aboutus_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getActivitylist')}}"><img src="/images/zh_TW/header/menu/btn_menu_activity.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_activity_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getCourselist')}}"><img src="/images/zh_TW/header/menu/btn_menu_course.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_course_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getPublication')}}"><img src="/images/zh_TW/header/menu/btn_menu_publishing.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_publishing_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getHeart')}}"><img src="/images/zh_TW/header/menu/btn_menu_heart.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_heart_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getProductlist',['id' => 0])}}"><img src="/images/zh_TW/header/menu/btn_menu_boutique.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_boutique_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getDonation')}}"><img src="/images/zh_TW/header/menu/btn_menu_donation.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_donation_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getContactus')}}"><img src="/images/zh_TW/header/menu/btn_menu_contactus.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_contactus_mo.png" class="imgButton" /></a>
                    </li>
                </ul>
            </div>
            <div id="divRightMenu" class="pull-right clear">
                <div class="imgButton">
                    <a href="https://www.facebook.com/CentreofMindfulness" target="_blank"><img src="/images/common/header/btn_facebook.png" class="imgButton" /></a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div id="divTopLine"></div>
</div>
<!-- 移动端导航 -->
<nav class="navbar navbar-default visible-xs-block navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{action('FrontController@getIndex')}}" style="float: none"><img src="/images/common/header/img_logo.png" /></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{action('FrontController@getAboutus')}}"><img src="/images/zh_TW/header/menu/btn_menu_aboutus.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_aboutus_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getActivitylist')}}"><img src="/images/zh_TW/header/menu/btn_menu_activity.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_aboutus_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getCourselist')}}"><img src="/images/zh_TW/header/menu/btn_menu_course.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_aboutus_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getPublication')}}"><img src="/images/zh_TW/header/menu/btn_menu_publishing.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_aboutus_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getHeart')}}"><img src="/images/zh_TW/header/menu/btn_menu_heart.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_aboutus_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getProductlist')}}"><img src="/images/zh_TW/header/menu/btn_menu_boutique.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_aboutus_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getDonation')}}"><img src="/images/zh_TW/header/menu/btn_menu_donation.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_aboutus_mo.png" class="imgButton" /></a>
                    </li>
                    <li>
                        <a href="{{action('FrontController@getContactus')}}"><img src="/images/zh_TW/header/menu/btn_menu_contactus.png" data-alt-src="/images/zh_TW/header/menu/btn_menu_aboutus_mo.png" class="imgButton" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>