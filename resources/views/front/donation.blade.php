@extends('front.layouts.default')

@section('page_css')
<link href="/styles/common/style_activitylist.css" rel="stylesheet" type="text/css" />
<link href="/styles/common/style_donation.css" rel="stylesheet" type="text/css" />
@stop


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-18">
            <div class="divMainTitle">
                <img src="/images/zh_TW/donation/title_donation.png">
            </div>        
        </div>
        <div class="col-md-18 divDonation">
            <div class="row divMessageBox">
                <div class="col-md-18">
                    <div class="divMessageBoxTop"></div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxContent">
                        <div>
                            供養佛像、護持中心、供養法師
                            <br> 捐款壹佰圓或以上，中心當奉寄收據，以作退税之用。
                        </div>
                        <div class="divSubTitle">
                            A. 支票
                        </div>
                        <div>
                            如蒙惠賜捐款，支票請抬頭「正念禪修中心有限公司」或 Centre of Mindfulness Limited。
                            <br> 請將支票郵寄往「香港荃灣海盛路3號TML廣場16樓D5室『正念禪修中心有限公司』」收。
                            <br> 請附上公德主姓名、電話及地址，以便發出收據。
                        </div>
                        <div class="divSubTitle">
                            B. 香港轉帳
                        </div>
                        <div>
                            如蒙惠賜捐款，請將款項存入「正念禪修中心有限公司」戶口：匯豐銀行 033-344722-001。
                            <br> 請將存款單公德主姓名、電話及地址寄回「香港荃灣海盛路3號TML廣場16樓D5室『正念禪修中心有限公司』」收，以便發出收據。
                            <br> 或把存款單拍照，連同公德主姓名、電話及地址電郵給中心（dhammamittahk@gmail.com），或Whatsapp／微信至中心電話（852-9697 2768），以便發出收據。
                        </div>
                        <div class="divSubTitle">
                            C. 海外惠捐
                        </div>
                        <div>
                            如蒙惠賜捐款，請將捐款匯款至中心帳戶，詳情如下：
                            <div class="table">
                                <div class="tableRow">
                                    <div class="tableCellTop">
                                        帳戶名稱　Account Name：
                                    </div>
                                    <div class="tableCellTop divTimeRightCell">
                                        正念禪修中心有限公司　The Centre of Mindfulness Limited
                                    </div>
                                </div>
                                <div class="tableRow">
                                    <div class="tableCellTop">
                                        帳戶號碼　Account No.：
                                    </div>
                                    <div class="tableCellTop divTimeRightCell">
                                        033-344722-001
                                    </div>
                                </div>
                                <div class="tableRow">
                                    <div class="tableCellTop">
                                        銀行名稱　Bank Name：
                                    </div>
                                    <div class="tableCellTop divTimeRightCell">
                                        匯豐銀行　HSBC Hong Kong
                                    </div>
                                </div>
                                <div class="tableRow">
                                    <div class="tableCellTop">
                                        銀行地址　Bank Address：
                                    </div>
                                    <div class="tableCellTop divTimeRightCell">
                                        香港中環皇后大道中1號　1 Queen's Road Central, Central, Hong Kong.
                                    </div>
                                </div>
                                <div class="tableRow">
                                    <div class="tableCellTop">
                                        SWIFT代碼：
                                    </div>
                                    <div class="tableCellTop divTimeRightCell">
                                        HSBCHKHHHKH
                                    </div>
                                </div>
                            </div>
                            護持匯款時，請註明捐贈功德款的目的為何：供養佛像／護持中心／供養法師；
                            <br> 並請在匯款後，電郵本中心（dhammamittahk@gmail.com）聯絡確認。
                        </div>
                    </div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxBottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="divPopup" class="container">
    <div id="divPopupInner">
        <div id="divPopupInnerArea" class="embed-responsive embed-responsive-4by3">
              <iframe class="embed-responsive-item" src="">Loading…</iframe>
        </div>
        <div class="text-center">
            <img class="imgButton divPopup_close" src="images/zh_TW/publication/btn_close.png" />
        </div>
    </div>
</div>
@stop

@section('page_js')
<script type="text/javascript">

$("#divPopup").popup({
    transition: 'all 0.3s'
});

function updatePopup(aObj) {
    var htmlCode = $(aObj).parent().find("input[type='hidden']").val();
    $("#divPopupInnerArea").find('iframe').attr('src', htmlCode);
}
</script>
@stop