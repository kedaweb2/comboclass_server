@extends('front.layouts.default')

@section('page_css')
<link href="/styles/common/style_activitylist.css" rel="stylesheet" type="text/css" />
<link href="/styles/common/style_productlist.css" rel="stylesheet" type="text/css" />
<link href="/styles/common/style_gallery.css" rel="stylesheet" type="text/css" />
@stop


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-18">
            <div class="divMainTitle">
                <img src="/images/zh_TW/productlist/title_boutique.png">
            </div>        
        </div>
        <div class="col-md-18">
            <div class="row divMessageBox">
                <div class="col-md-18">
                    <div class="divMessageBoxTop"></div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxContent">
                        <div class="row">
                            <div class="col-xs-6 col-sm-2  divCategoryCell">
                                @foreach ($gallerylist as $item)                                
                                <div class="alignLeft divCategory @if($id != $item->id) noArrow @endif">
                                    @if($id == $item->id)
                                    <div class="tableCellMiddle" style="padding:0 5px 0 0; width:15px;">
                                        <img src="/images/common/productlist/img_boutique_menu_arrow.png">
                                    </div>
                                    @endif
                                    <div class="tableCellMiddle">
                                        <a href="{{action('FrontController@getGallery', ['id' => $item->id])}}"><div class="break-word">{{$item->name_cht}}</div></a>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="col-xs-12 col-sm-16">
                                <div id="carousel-example-generic" class="carousel slide slideshow" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators hidden">
                                        @foreach ($list as $key => $photo)
                                        @if($key == 0)
                                        <li data-target="#carousel-example-generic" data-slide-to="{{$key}}" class="active"></li>
                                        @else
                                        <li data-target="#carousel-example-generic" data-slide-to="{{$key}}"></li>
                                        @endif
                                        @endforeach
                                    </ol>
                                    <div class="carousel-inner" role="listbox">
                                        @foreach ($list as $key => $photo)
                                        @if($key == 0)
                                        <div class="item active">
                                            <a href="#"><img class="center-block" src="{{url($photo->photo)}}" style="cursor:pointer;" /></a>
                                        </div>
                                        @else
                                        <div class="item">
                                            <a href="#"><img class="center-block" src="{{url($photo->photo)}}" style="cursor:pointer;" /></a>
                                        </div>
                                        @endif
                                        @endforeach
                                    </div>

                                    <!-- Controls -->
                                      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                </div>
                                {{-- <div id="albumDiv" class="slideshow"><img src="./images/common/gallery/dummy.jpg" style="height: 376px; width: 625px;"></div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxBottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page_js')
{{-- <link rel="stylesheet" href="/javascripts/slideshow/styles/slideshow.css" /> --}}
{{-- <script src="/javascripts/slideshow/mootools-1.3.2-core.js"></script> --}}
{{-- <script src="/javascripts/slideshow/mootools-1.3.2.1-more.js"></script> --}}
{{-- <script src="/javascripts/slideshow/slideshow.js"></script> --}}
{{-- <script src="/javascripts/slideshow/slideshow.flash.js"></script> --}}
{{-- <script src="/javascripts/slideshow/slideshow.fold.js"></script> --}}
{{-- <script src="/javascripts/slideshow/slideshow.kenburns.js"></script> --}}
{{-- <script src="/javascripts/slideshow/slideshow.push.js"></script> --}}
<script type="text/javascript">

// window.addEvent('domready', function(){
//     var data = {};
//     new Slideshow.Push('albumDiv', data, { duration: 3000, width: 625, height: 376, hu: './files/gallery/', transition: 'back:in:out'});
// });

</script>
@stop