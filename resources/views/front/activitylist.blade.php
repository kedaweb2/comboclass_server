@extends('front.layouts.default')

@section('page_css')
<link href="/styles/common/style_activitylist.css" rel="stylesheet" type="text/css" />
@stop


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-18">
            <div class="divMainTitle">
                <img src="/images/zh_TW/activity/title_activity.png">
            </div>        
        </div>
        <div class="col-md-18">
            <div class="row divMessageBox">
                <div class="col-md-18">
                    <div class="divMessageBoxTop"></div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxContent">
                        @foreach ($activitylist as $key => $item)
                        <div class="divActivityTable">
                            <div class="row divActivityBlock">
                                <div class="col-sm-4 divActivityImageCell">
                                    <div class="divActivityImage pull-left"><a href="{{action('FrontController@getActivitydetail', ['id' => $item->id])}}"><img class="img-responsive" src="{{$item->photo}}" /></a></div>
                                </div>
                                <div class="col-sm-14">
                                    <div class="divActivityTitle row">
                                        <div class="col-sm-15"><a href="{{action('FrontController@getActivitydetail', ['id' => $item->id])}}">{{$item->title_cht}}</a></div>
                                        <div class="col-sm-3">
                                            @if(!empty($item->form))
                                            <input type="hidden" id="view1:form:j_id_p:1:j_id_w" name="view1:form:j_id_p:1:j_id_w" value="{{$item->form}}" />
                                            <img src="/images/zh_TW/activity/btn_register.png" class="imgButton divPopup_open" onclick="updatePopup(this);" data-popup-ordinal="0" id="open_28387106">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="divActivityDesc1">{!!$item->intro_cht!!}<br /> &nbsp;</div>
                                    @if($item->ActivityItem->count() > 0)
                                    <div class="divAttachments">
                                        <div class="table">
                                            <div class="tableRow">
                                                <div class="tableCellMiddle" style="width:50px;">
                                                    <img src="./images/common/activity/img_pdf_icon.png">
                                                </div>
                                                <div class="tableCellMiddle" style="width:50px;">下載：
                                                </div>
                                                <div class="tableCellMiddle">
                                                    @foreach ($item->ActivityItem as $aItem)  
                                                    <div class="divDownloadLink">
                                                        <a href="{{url($aItem->file)}}" target="_blank">{{$aItem->name}}</a>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row divActivitySeparator">
                            <img class="img-responsive" src="./images/common/activity/img_activity_line.png">
                        </div>
                        @endforeach
                        <div id="divPagination" class="text-center">
                            {!! $activitylist->render() !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxBottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="divPopup" class="container">
    <div id="divPopupInner">
        <div id="divPopupInnerArea" class="embed-responsive embed-responsive-4by3">
              <iframe class="embed-responsive-item" src="">Loading…</iframe>
        </div>
        <div class="text-center">
            <img class="imgButton divPopup_close" src="images/zh_TW/publication/btn_close.png" />
        </div>
    </div>
</div>
@stop

@section('page_js')
<script type="text/javascript">

$("#divPopup").popup({
    transition: 'all 0.3s'
});

function updatePopup(aObj) {
    var htmlCode = $(aObj).parent().find("input[type='hidden']").val();
    $("#divPopupInnerArea").find('iframe').attr('src', htmlCode);
}
</script>
@stop