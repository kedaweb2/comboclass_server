@extends('front.layouts.default')

@section('page_css')
<link href="/styles/common/style_activitylist.css" rel="stylesheet" type="text/css" />
<link href="/styles/common/style_heart.css" rel="stylesheet" type="text/css" />
@stop


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-18">
            <div class="divMainTitle">
                <img src="/images/zh_TW/heart/title_heart.png">
            </div>        
        </div>
        <div class="col-md-18">
            <div class="row divMessageBox">
                <div class="col-md-18">
                    <div class="divMessageBoxTop"></div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxContent">
                        <div class="divBanner">
                            <img class="img-responsive" src="./images/common/heart/img_heart_banner.png">
                        </div>
                        <div class="divBannerMessage">
                            心靈聆聽在清雅的環境中，心神平靜，情緒放鬆，暫時放下擔憂，說出您的疑惑，讓法師細心聆聽並為您祝福。您可與一位參與活動的法師面談，內容全部保密。
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>法師</th>
                                    <th>日期</th>
                                    <th>時段</th>
                                    <th>狀態</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($list as $key => $item)
                                <tr>
                                    <td>{{$item->speaker_cht}}</td>
                                    <td>{{$item->heart_date}}</td>
                                    <td>{{$item->start_time}}至{{$item->end_time}}</td>
                                    <td>
                                        @if($item->reserved)
                                        <img src="/images/zh_TW/heart/btn_heart_booked.png" >
                                        @else
                                        <img id="js_bookBtn_{{$item->id}}" src="/images/zh_TW/heart/btn_heart_book.png" class="imgButton divPopupEnquiry_open" onclick="updateEnquiry({{$item->id}});">
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxBottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="divPopupEnquiry" class="container">
    <div id="divPopupEnquiryInner">
        <div class="divPopupTitle">填寫預約人資料</div>
        <form class="form-horizontal" id="enquiryform">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">姓氏</label>
                <div class="col-sm-14">
                    <input type="hidden" name="field1" value="姓氏" />
                    <input type="text" name="name" size="40" id="name" />
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">電郵地址</label>
                <div class="col-sm-14">
                    <input type="text" name="email" size="40" id="email" />
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">聯絡電話</label>
                <div class="col-sm-14">
                    <input type="text" name="phone" size="40" id="phone" />
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">性別</label>
                <div class="col-sm-14">
                    <select name="gender" id="gender">
                        <option value="男">男</option>
                        <option value="女">女</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-18 text-center">
                    <input type="hidden" id="js_heartId" name="id">
                    <img class="imgButton" src="./images/zh_TW/heart/btn_heart_register.png" onclick="submitEnquiry();" />
                    <img class="imgButton divPopupEnquiry_close" src="./images/zh_TW/heart/btn_heart_cancel.png" />
                </div>
            </div>
        </form>
    </div>
</div>
@stop

@section('page_js')
<script type="text/javascript">

$("#divPopupEnquiry").popup({
    transition: 'all 0.3s'
});

function updateEnquiry(heartId) {
    $('#js_heartId').val(heartId);
}

function submitEnquiry() {
    if (validateEnquiry()) {
        $.ajax({
            url: "{{action('FrontController@postHeartBook')}}",
            type: 'POST',
            dataType: 'json',
            data: $("#enquiryform").serialize(),
        })
        .done(function(data) {
            $("#loadingDiv").fadeOut(200);
            if (data.error_code === 0) {
                clearEnquiry();
                $('#divPopupEnquiry').popup('hide');
                alert("我們會盡快處理預約");
                location.reload();
            } else {
                $('#divPopupEnquiry').popup('hide');
                alert(data.info);
            }
        })
        .fail(function() {
            clearEnquiry();
            $('#divPopupEnquiry').popup('hide');
            alert("請直接跟本中心聯絡");
        })
    }
    return false;
}
function validateEnquiry() {
    var result = true;
    if ( isBlankString($("#name").val()) ) {
        alert("請填上姓名");
        result = false;
    } else if ( isBlankString($("#email").val()) || validateEmail($("#email").val())==false ) {
        alert("請填上正確的電郵地址");
        result = false;
    } else if ( isBlankString($("#phone").val()) ) {
        alert("請填上聯絡電話");
        result = false;
    }
    return result;
}
function clearEnquiry() {
    $("#name").val("");
    $("#email").val("");
    $("#phone").val("");
}
function updateButton() {
    var id = $("#js_heartId").val();
    var $js_bookBtn = $("#js_bookBtn_"+id);
    var src = $js_bookBtn.attr('src');
    src.replace('btn_heart_book', 'btn_heart_booked');
    $js_bookBtn.attr('src', src);
}
</script>
@stop