@extends('front.layouts.default')

@section('page_css')
<link href="/styles/common/style_activitylist.css" rel="stylesheet" type="text/css" />
<link href="/styles/common/style_activity.css" rel="stylesheet" type="text/css" />
@stop


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-18">
            <div class="divMainTitle">
                <img src="/images/zh_TW/course/title_course.png">
            </div>        
        </div>
        <div class="col-md-18">
            <div class="row divMessageBox">
                <div class="col-md-18">
                    <div class="divMessageBoxTop"></div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxContent">
                        <div class="divActivityBlock divDetailBlock">
                            <div class="divActivityTable">
                                <div class="row">
                                    <div class="col-sm-14">
                                        <div class="divActivityTitle row">
                                            <div class="col-sm-11 col-md-14">{{$detail->title_cht}}</div>
                                            <div class="col-sm-3">
                                                @if(!empty($detail->form))
                                                <input type="hidden" id="view1:form:j_id_p:1:j_id_w" name="view1:form:j_id_p:1:j_id_w" value="{{$detail->form}}" />
                                                <img src="/images/zh_TW/activity/btn_register.png" class="imgButton divPopup_open" onclick="updatePopup(this);" data-popup-ordinal="0" id="open_28387106">
                                                @endif
                                            </div>
                                        </div>
                                        <div class="divActivityDesc1">{!!$detail->intro_cht!!}</div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="divActivityImageCell">
                                            <div class="divActivityImage divPopup2_open" onclick="updatePopupImage(this);">
                                                <img src="{{url($detail->photo)}}" class="imgActivityImage img-responsive">
                                                <div class="divZoomImage"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($detail->ActivityItem->count() > 0)
                                <div class="divAttachments">
                                    <div class="tableRow">
                                        <div class="tableCellMiddle" style="width:50px;">
                                            <img src="/images/common/activity/img_pdf_icon.png">
                                        </div>
                                        <div class="tableCellMiddle" style="width:50px;">下載：</div>
                                        <div class="tableCellMiddle">
                                            @foreach ($detail->ActivityItem as $item)                                            
                                            <div class="divDownloadLink">
                                                <a href="{{url($item->file)}}" target="_blank">{{$item->name}}</a>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="divActivityDetail">
                                <div class="divActivityDetailTitle">活動詳情：</div>
                                <div class="divActivityDetailContent">課程內容：
                                    {!!$detail->detail_cht!!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxBottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="divPopup" class="container">
    <div id="divPopupInner">
        <div id="divPopupInnerArea" class="embed-responsive embed-responsive-4by3">
              <iframe class="embed-responsive-item" src="">Loading…</iframe>
        </div>
        <div class="text-center">
            <img class="imgButton divPopup_close" src="/images/zh_TW/publication/btn_close.png" />
        </div>
    </div>
</div>

<div id="divPopup2">
    <div id="divPopupInner">
        <div style="text-align:center; padding:0;">
            <img id="imgPopupImageLarge" />
            <div style="position:absolute; top:-16px; right:-16px;">
                <img class="imgButton divPopup2_close" src="/images/common/common/btn_close.png" style="width:32px;" />
            </div>
        </div>
    </div>
</div>
@stop

@section('page_js')

<script type="text/javascript">

$("#divPopup").popup({
    transition: 'all 0.3s'
});

$("#divPopup2").popup({
    transition: 'all 0.3s'
});

function updatePopupImage(aObj) {
    var imgSrc = $(aObj).find("img[class*='imgActivityImage']").attr("src");
    // imgSrc = imgSrc.replace("large", "original");
    $("#imgPopupImageLarge").attr("src", imgSrc);
    console.log(imgSrc);
}

function updatePopup(aObj) {
    var htmlCode = $(aObj).parent().find("input[type='hidden']").val();
    $("#divPopupInnerArea").find('iframe').attr('src', htmlCode);
}
</script>
@stop