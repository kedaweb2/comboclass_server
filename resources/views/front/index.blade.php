@extends('front.layouts.default')

@section('page_css')
<link href="/styles/common/style_index.css" rel="stylesheet" type="text/css" />
@stop

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-11">
            <div class="row divMessageBoxMini">
                <div class="col-md-18 divMessageBoxTop"></div>
                <div class="col-md-18 divMessageBoxContent">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            @foreach ($banners as $key => $banner)
                            @if($key == 0)
                            <li data-target="#carousel-example-generic" data-slide-to="{{$key}}" class="active"></li>
                            @else
                            <li data-target="#carousel-example-generic" data-slide-to="{{$key}}"></li>
                            @endif
                            @endforeach
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            @foreach ($banners as $key => $banner)
                            @if($key == 0)
                            <div class="item active">
                                <a href="{{$banner->url}}" title="{{$banner->title_cht}}"><img src="{{url($banner->photo)}}" style="cursor:pointer;" alt="{{$banner->title_cht}}" title="{{$banner->title_cht}}" /></a>
                            </div>
                            @else
                            <div class="item">
                                <a href="{{$banner->url}}" title="{{$banner->title_cht}}"><img src="{{url($banner->photo)}}" style="cursor:pointer;" alt="{{$banner->title_cht}}" title="{{$banner->title_cht}}" /></a>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-18 divMessageBoxBottom"></div>
            </div>
            <div class="row">
                <div class="col-md-18">
                    <div class="divMainTitle" style="position:relative;">
                        <img src="images/zh_TW/index/title_index_activity.png" />
                        <div class="divMainTitleMore">
                            <a href="{{action('FrontController@getActivitylist')}}"><img src="images/zh_TW/index/btn_more.png" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row divMessageBoxMini">
                <div class="col-md-18 divMessageBoxTop"></div>
                <div class="col-md-18 divMessageBoxContent">
                    @foreach ($activitylist as $key => $item)
                    <div class="row divActivityBlock">
                        <div class="col-xs-4 divActivityImageCell">
                            <div class="divActivityImage"><a href="{{action('FrontController@getActivitydetail', ['id' => $item->id])}}"><img src="{{url($item->photo)}}" /></a></div>
                        </div>
                        <div class="col-xs-14">
                            <div class="divActivityTitle">
                                <a href="{{action('FrontController@getActivitydetail', ['id' => $item->id])}}">{{$item->title_cht}}</a>
                            </div>
                            <div class="divActivityDesc1">
                                {!!$item->index_intro_cht!!}
                                <br /> &nbsp;
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="col-md-18 divMessageBoxBottom"></div>
            </div>
            <div class="row">
                <div class="col-md-18">
                    <div class="divMainTitle" style="position:relative;">
                        <img src="images/zh_TW/index/title_index_course.png" />
                        <div class="divMainTitleMore">
                            <a href="{{action('FrontController@getCourselist')}}"><img src="images/zh_TW/index/btn_more.png" /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row divMessageBoxMini">
                <div class="col-md-18 divMessageBoxTop"></div>
                <div class="col-md-18 divMessageBoxContent">
                    @foreach ($courselist as $key => $item)
                    <div class="row divActivityBlock">
                        <div class="col-xs-4 divActivityImageCell">
                            <div class="divActivityImage"><a href="{{action('FrontController@getCoursedetail', ['id' => $item->id])}}"><img src="{{url($item->photo)}}" /></a></div>
                        </div>
                        <div class="col-xs-14">
                            <div class="divActivityTitle">
                                <a href="{{action('FrontController@getCoursedetail', ['id' => $item->id])}}">{{$item->title_cht}}</a>
                            </div>
                            <div class="divActivityDesc1">
                                {!!$item->index_intro_cht!!}
                                <br /> &nbsp;
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="col-md-18 divMessageBoxBottom"></div>
            </div>
        </div>
        <div class="col-md-7">
            <!-- Facebook -->
            <div class="fb-page" data-href="https://www.facebook.com/CentreofMindfulness" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-height="970"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/CentreofMindfulness"><a href="https://www.facebook.com/CentreofMindfulness">CentreofMindfulness Facebook</a></blockquote></div></div>
        </div>
    </div>
</div>
@stop

@section('page_js')

<div id="fb-root"></div>
<div id="fb-root"></div>
<script type="text/javascript">
$(document).ready(function() {

    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=144161609068665";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // $('#slides').slidesjs({
    //     width: 500,
    //     height: 268,
    //     play: {
    //         active: false,
    //         /*effect: "fade",*/
    //         auto: true,
    //         interval: 4000,
    //         swap: true
    //     },
    //     navigation: {
    //         active: false,
    //     }
    // });

});
</script>
@stop