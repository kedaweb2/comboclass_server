@extends('front.layouts.default')

@section('page_css')
<link href="/styles/common/style_activitylist.css" rel="stylesheet" type="text/css" />
<link href="/styles/common/style_publication.css" rel="stylesheet" type="text/css" />
@stop


@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-18">
            <div class="divMainTitle">
                <img src="/images/zh_TW/publication/title_publishing.png">
            </div>        
        </div>
        <div class="col-md-18">
            <div class="row divMessageBox">
                <div class="col-md-18">
                    <div class="divMessageBoxTop"></div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxContent">
                        <div class="">                        
                            <div class="col-sm-1"></div>
                            @foreach ($list as $key => $item)
                            <div class="col-sm-4">
                                <div class="divPublicationBlock">
                                    <div class="divPublicationImage"><img src="{{url($item->photo)}}" onclick="updatePopup(this, '{{url($item->photo)}}', '{{url($item->ebook)}}')" class="imgButton divPopup_open img-responsive"></div>
                                    <div class="divPublicationTitle">{{$item->title_cht}}</div>
                                    <div class="divPublicationDesc1">{{$item->author_name_cht}}</div>
                                    <input type="hidden" id="view1:form:j_id_p:0:j_id_z" name="view1:form:j_id_p:0:j_id_z" value="{!!$item->book_intro_cht!!} <br> 歡迎免費到中心索取">
                                </div>
                            </div>
                            @endforeach
                            <div class="col-sm-1"></div>
                        </div>
                        <div id="divPagination" class="text-center">
                            {!! $list->render() !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-18">
                    <div class="divMessageBoxBottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="divPopup" class="container">
    <div id="divPopupInner">
        <div class="table" style="width:100%;">
            <div class="tableRow">
                <div class="tableCellTop" style="padding:0 20px 0 0;">
                    <div id="divPopupTitle"></div>
                    <div id="divPopupDesc1">
                        <span>作者名稱：</span><span id="spanPopupDesc1"></span>
                    </div>
                    
                    <div id="divPopupBookDesc2Container">
                        <div id="divPopupBookDesc2Title">
                            書籍介紹：
                        </div>
                        <div id="divPopupBookDesc2"></div>
                    </div>
                </div>
                <div class="tableCellTop">
                    <div id="divPopupImage" onclick="showLargeImage(this);">
                        <img id="imgPopupImage" class="img-responsive" />
                        <div class="divZoomImage"></div>
                    </div>
                    <div id="divPopupBook">
                        <a target="_blank" id="aPopupBook">下載電子書</a>
                    </div>
                </div>
            </div>
        </div>
        <div style="text-align:center; padding:20px 0 0 0;">
            <img class="imgButton divPopup_close" src="images/zh_TW/publication/btn_close.png" />
        </div>
    </div>
    <div id="divPopupInnerImage" style="display:none;">
        <div style="text-align:center; padding:20px 0 0 0;">
            <img id="imgPopupImageLarge" />
        </div>
        <div style="text-align:center; padding:20px 0 0 0;">
            <img class="imgButton" src="images/zh_TW/publication/btn_close.png" onclick="hideLargeImage();" />
        </div>
    </div>
</div>
@stop

@section('page_js')
<script type="text/javascript">

$("#divPopup").popup({
    transition: 'all 0.3s'
});

function updatePopup(aObj, aImgSrc, aBookSrc) {
                
    hideLargeImage();
    
    var title = $(aObj).parent().parent().find(".divPublicationTitle").html().trim();
    $("#divPopupTitle").html(title);
    
    var desc1 = $(aObj).parent().parent().find(".divPublicationDesc1").html().trim();
    if (isBlankString(aBookSrc)==false) {
        $("#divPopupDesc1").show();
        $("#spanPopupDesc1").html(desc1);
    } else {
        $("#divPopupDesc1").hide();
    }
    
    var desc2 = $(aObj).parent().parent().find("input[type='hidden']").val();
    if (isBlankString(desc2)==false) {
        $("#divPopupBookDesc2Container").show();
        $("#divPopupBookDesc2").html(desc2);
    } else {
        $("#divPopupBookDesc2Container").hide();
    }
    
    $("#imgPopupImage").attr("src", aImgSrc);
    if (isBlankString(aBookSrc)==false) {
        $("#divPopupBook").show();
        $("#aPopupBook").attr("href", aBookSrc);
    } else {
        $("#divPopupBook").hide();
    }
}

function showLargeImage(aObj) {
    var imgSrc = $(aObj).find("img").attr("src");
    // imgSrc = imgSrc.replace("large", "original");
    $("#imgPopupImageLarge").attr("src", imgSrc);
    
    $("#divPopupInner").slideUp(400);
    $("#divPopupInnerImage").slideDown(400);
}

function hideLargeImage() {
    $("#divPopupInnerImage").slideUp(400);
    $("#divPopupInner").slideDown(400);
}
</script>
@stop