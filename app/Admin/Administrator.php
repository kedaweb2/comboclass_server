<?php

/*
 * This is a simple example of the main features.
 * For full list see documentation.
 */

Admin::model(\App\Model\Administrator::class)->title('Users')->display(function ()
{
    $display = AdminDisplay::table();
    $display->columns([
        Column::string('name')->label('Name'),
        Column::string('username')->label('Username'),
    ]);
    return $display;
})->createAndEdit(function ($id)
{
    $form = AdminForm::form();
    
    $formItems = [
        FormItem::text('name', 'Name')->required(),
        FormItem::text('username', 'Username')->required()->unique(),
    ];

    $formItems[] = is_null($id) ? FormItem::text('password', 'Password')->required() : FormItem::text('password', 'Password');

    $form->items($formItems);
    return $form;
})->delete(function ($id) {
    if ($id == 1) {
        return null;
    }
    return true;
});