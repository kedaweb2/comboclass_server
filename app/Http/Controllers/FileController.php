<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

use App\Http\Requests;
use App\Http\Controllers\BaseController;
use App\Model\CourseItem;

class FileController extends BaseController
{
    protected $upload_root;

    public function __construct()
    {
        $this->upload_root = public_path('uploads/');
    }

    public function upload(Request $request)
    {
        $yourDir = $request->input('dir');
        $display_name = $request->input('name');

        $files = $request->file('files');
        $filesInfo = [];
        foreach ($files as $key => $file) {
            $tmp = $this->move_file($file, $yourDir);
            $tmp['name'] = $display_name ? $display_name : $tmp['file_name'];
            $filesInfo['files'][] = $tmp;
        }
        return response()->json($filesInfo);
    }

    private function move_file($file, $yourDir = null)
    {
        $clientName = $file->getClientOriginalName();
        $entension = $file->getClientOriginalExtension();

        $newName = md5(date('ymdhis').$clientName).".".$entension;
        $destinationPath = public_path('uploads/' . $yourDir);
        if(!File::exists($destinationPath)) {
            File::makeDirectory($destinationPath);
        }

        $file_path = $file->move($destinationPath, $newName) ? '/uploads/'.$newName : "";
        $file_info['file_name'] = $newName;
        $file_info['url'] = url($file_path);
        $file_info['path'] = $file_path;
        $file_info['size'] = $file->getSize();
        return $file_info;
    }

    public function del(Request $request)
    {
        $fileUrl = $request->input('fileUrl');
        CourseItem::where('file', $fileUrl)->delete();
        File::delete(public_path($fileUrl));
        return response()->json([true]);
    }
}
