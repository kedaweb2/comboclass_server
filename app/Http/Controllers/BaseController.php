<?php

namespace App\Http\Controllers;

use URL;
use Route;

use Intervention\Image\ImageManagerStatic as Image;

class BaseController extends Controller
{
    public function __construct()
    {
        //獲取當前控制器
        $route = Route::currentRouteAction();
        list($this->controller, $action) = explode('@', $route);
        $this->controller = last(explode('\\', $this->controller));

        view()->share('controller', $this->controller);
        view()->share('action', $action);
    }

    public function resourceCtrlInit($ctrl_name_space)
    {
        //獲取當前控制器
        $route = Route::currentRouteAction();
        list($this->controller, $action) = explode('@', $route);
        $this->controller = $ctrl_name_space.'\\'.last(explode('\\', $this->controller));
        $this->indexUrl = action($this->controller . '@index');
        view()->share('controller', $this->controller);
        view()->share('indexUrl', $this->indexUrl);
        view()->share('action', $action);
    }

    protected function _upload_file($file_name, $request)
    {
        $dirname = property_exists($this, 'dirname') ? $this->dirname."/" : '';
        $file_path = "";

        if( $request->hasFile($file_name) ){
            $file = $request->file($file_name);

            $clientName = $file->getClientOriginalName();
            $entension = $file->getClientOriginalExtension();

            $newName = md5(date('ymdhis').$clientName).".".$entension;
            $destinationPath = public_path('uploads/'.$dirname);
            File::mkdir($destinationPath);
            $file_path = $file->move($destinationPath, $newName) ? '/uploads/'.$dirname.$newName : "";
        }
        return $file_path;
    }

    protected function _resizeImage($file, $targetWidth, $targetHeight)
    {
        $file_path = public_path().$file;
        
        $targetRatio = $targetWidth / $targetHeight;

        $sourceWidth = Image::make($file_path)->width(); // Soruce Width
        $sourceHeight = Image::make($file_path)->height(); // Soruce Height
        $sourceRatio = $sourceWidth / $sourceHeight;

        if ( $sourceRatio < $targetRatio ) {
            $scale = $sourceWidth / $targetWidth;
        } else {
            $scale = $sourceHeight / $targetHeight;
        }

        $resizeWidth = (int)($sourceWidth / $scale);
        $resizeHeight = (int)($sourceHeight / $scale);

        $cropLeft = (int)(($resizeWidth - $targetWidth) / 2);
        $cropTop = (int)(($resizeHeight - $targetHeight) / 2);

        Image::make($file_path)->resize($resizeWidth, $resizeHeight)->crop($targetWidth, $targetHeight, $cropLeft, $cropTop)->save($file_path);
    }
}
